// Warewolf

console.log('_Soal Warewolf_')
//1
var nama0 = "" ;
var peran0 = "" ;

if (nama0 == "" && peran0 == "") {
    console.log ("Nama harus diisi!")
}

//2
var nama1 = 'John';
var peran1 = '';

if (nama1 == '') {
    console.log('Nama harus diisi!')
}
else {
    console.log ('Halo, ' + nama1 + '. Pilih peranmu untuk memulai game!')
}

//3
var nama2 = "Jane"
var peran2 = "Penyihir"

if (nama2 != "") {
    console.log ("Selamat datang di Dunia Warewolf, " + nama2 + ".")
}


if (peran2 === "Penyihir") {
        console.log ("Halo, " + peran2 + " " + nama2 + ". Kamu dapat melihat siapa yang menjadi warewolf!")
    }
else if (peran2 === "Guard") {
        console.log ("Halo, " + peran2 + " " + nama2 + ". Kamu akan membantu melindungi temanmu dari serangan warewolf.")
    }
else if (peran2 === "Warewolf") {
        console.log ("Halo, " + peran2 + " " + nama2 + ". Kamu akan memakan mangsa setiap malam")
    
}

//4
var nama3 = "Jenita"
var peran3 = "Guard"

if (nama2 != "") {
    console.log ("Selamat datang di Dunia Warewolf, " + nama3 + ".")
}

if  (peran3 !== "Warewolf" && peran3 !== "Guard") {
        console.log ("Halo, " + peran3 + " " + nama3 + ". Kamu dapat melihat siapa yang menjadi warewolf!")
    }

else if (peran3 !== "Penyihir" && peran3 !== "Warewolf") {
        console.log ("Halo, " + peran3 + " " + nama3 + ". Kamu akan membantu melindungi temanmu dari serangan warewolf.")
    }

else if (peran3 !== "Guard" && peran3 !== "Penyihir") {
        console.log ("Halo, " + peran3 + " " + nama3 + ". Kamu akan memakan mangsa setiap malam")
}

//5
var nama4 = "Junaedi"
var peran4 = "Warewolf"

if (nama4 != "") {
    console.log ("Selamat datang di Dunia Warewolf, " + nama4 + ".")
}


if (peran4 === "Penyihir") {
        console.log ("Halo, " + peran4 + " " + nama4 + ". Kamu dapat melihat siapa yang menjadi warewolf!")
    }
else if (peran4 === "Guard") {
        console.log ("Halo, " + peran4 + " " + nama4 + ". Kamu akan membantu melindungi temanmu dari serangan warewolf.")
    }
else if (peran4 === "Warewolf") {
        console.log ("Halo, " + peran4 + " " + nama4 + ". Kamu akan memakan mangsa setiap malam.")
    
}

// SwitchCase

console.log('_Soal Switch Case_')

//input
var bulan = 9


switch (bulan) {
    case 1:   bulan = ' Januari '; break; 
    case 2:   bulan = ' Februari '; break; 
    case 3:   bulan = ' Maret '; break; 
    case 4:   bulan = ' April '; break; 
    case 5:   bulan = ' Mei '; break; 
    case 6:   bulan = ' Juni '; break; 
    case 7:   bulan = ' Juli '; break; 
    case 8:   bulan = ' Agustus '; break; 
    case 9:   bulan = ' September '; break; 
    case 10:  bulan = ' Oktober '; break; 
    case 11:  bulan = ' November '; break; 
    case 12:  bulan = ' Desember '; break; 
    default: {console.log('Salah Bulan')}
}

//input
var tanggal = 28
var tahun = 1999

if (tanggal <= 1 || tanggal >= 31) {
    console.log ('Tanggal salah')
}
else if (tahun <= 1900 || tahun >= 2200) {
    console.log('Tahun di luar rentang')
}
else
{
    console.log(tanggal + " " + bulan + " " + tahun)
}




