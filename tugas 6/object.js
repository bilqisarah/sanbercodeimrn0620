/*
Buatlah function dengan nama arrayToObject() yang menerima sebuah parameter berupa array multidimensi. 
Dalam array tersebut berisi value berupa First Name, Last Name, Gender, dan Birthyear. 
Data di dalam array dimensi tersebut ingin kita ubah ke dalam bentuk Object dengan 
key bernama : firstName, lastName, gender, dan age. Untuk key age ambillah selisih tahun yang ditulis 
di data dengan tahun sekarang. Jika tahun tidak terdefinisi atau tahunnya lebih besar dibandingkan dengan 
tahun sekarang maka kembalikan nilai : “Invalid birth year”.

Contoh: jika input nya adalah [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]

maka outputnya di console seperti berikut :

1. Abduh Muhamad : { firstName: "Abduh", lastName: "Muhamad", gender: "male", age: 28}
2. Ahmad Taufik : { firstName: "Ahmad", lastName: "Taufik", gender: "male", age: 35} 
Untuk mendapatkan tahun sekarang secara otomatis bisa gunakan Class Date dari Javascript.
*/ 


var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)
//console.log(thisYear)

var arr = [["Abduh", "Muhamad", "male", 1992], 
                ["Ahmad", "Taufik", "male", 1985]]

function arrayToObject(arr) {
    for (i = 0; i < arr.length-1; i++)
    arr.firstName = arr[0][i]
    arr.lastName = arr[0][i] 
    arr.gender = arr[0][i+1]
    arr.age = arr[0][i+2]
    arr.firstName2 = arr[1][i-1]
    arr.lastName2 = arr[1][i] 
    arr.gender2 = arr[1][i+1]
    arr.age2 = arr[1][i+2]
    return i
}
 
arrayToObject(arr) 
console.log ('1. Abduh Muhamad: ')
console.log('firstName:',arr.firstName)
console.log('lastName: ',arr.lastName)
console.log ('gender:',arr.gender)
console.log('age: ',arr.age)
console.log ('2. Ahmad Taufik ')
console.log('firstName:',arr.firstName2)
console.log('lastName: ',arr.lastName2)
console.log ('gender:',arr.gender2)
console.log('age: ',arr.age2)


var people = [ ["Bruce", "Banner", "male",1975], 
["Natasha", "Romanoff", "female", ""] ]


function arrayToObject(people) {
    for (i = 0; i < people.length-1; i++)
    var tahun = people[0][i+2]
    var tahun2 = people[1][i+2]
    if(tahun>thisYear) {
        console.log('Invalid Birth Year')
    }
    
    if(tahun2>thisYear) {
        console.log('Invalid Birth Year')
    }
    else if (tahun2==="") {
    console.log('Invalid Birth Year')
    }

    people.firstName = people[0][i]
    people.lastName = people[0][i] 
    people.gender = people[0][i+1]
    people.age = people[0][i+2]
    people.firstName2 = people[1][i-1]
    people.lastName2 = people[1][i] 
    people.gender2 = people[1][i+1]
    people.age2 = people[1][i+2]

    return i
}
 
arrayToObject(people) 
console.log ('1. Bruce Banner: ')
console.log('firstName:',people.firstName)
console.log('lastName: ',people.lastName)
console.log ('gender:',people.gender)
console.log('age: ',people.age)
console.log ('2. Natasha Rumanof ')
console.log('firstName:',people.firstName2)
console.log('lastName: ',people.lastName2)
console.log ('gender:',people.gender2)
console.log('age: ',people.age2)



        
        
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 