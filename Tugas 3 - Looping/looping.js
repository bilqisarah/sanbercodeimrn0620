//Soal 1 (WHILE)
/* OUTPUT
LOOPING PERTAMA
2 - I love coding
4 - I love coding
6 - I love coding
8 - I love coding
10 - I love coding
12 - I love coding
14 - I love coding
16 - I love coding
18 - I love coding
20 - I love coding
LOOPING KEDUA
20 - I will become a mobile developer
18 - I will become a mobile developer                                                                              
16 - I will become a mobile developer
14 - I will become a mobile developer
12 - I will become a mobile developer
10 - I will become a mobile developer
8 - I will become a mobile developer
6 - I will become a mobile developer
4 - I will become a mobile developer
2 - I will become a mobile developer 
*/

console.log('__Nomor 1__')

var wloop1 = 2
console.log('LOOPING PERTAMA')

while (wloop1 <= 20 ){
    console.log(wloop1 + ' - I love coding!')
    wloop1 += 2
}

var wloop2 = 20
console.log('LOOPING KEDUA')
while (wloop2 >= 2) {
    console.log(wloop2 + ' - I will become a mobile developer!')
    wloop2 -= 2
}


//Soal 2  (FOR)
/*
SYARAT:
A. Jika angka ganjil maka tampilkan Santai
B. Jika angka genap maka tampilkan Berkualitas
C. Jika angka yang sedang ditampilkan adalah kelipatan 3 
DAN angka ganjil maka tampilkan I Love Coding.

OUTPUT 
1 - Santai
2 - Berkualitas
3 - I Love Coding 
4 - Berkualitas
5 - Santai
6 - Berkualitas
7 - Santai
8 - Berkualitas
9 - I Love Coding
10 - Berkualitas
11 - Santai
12 - Berkualitas
13 - Santai
14 - Berkualitas
15 - I Love Coding
16 - Berkualitas
17 - Santai
18 - Berkualitas
19 - Santai
20 - Berkualitas
*/

console.log ('__Nomor 2__')
console.log ('OUTPUT')
for (y = 1; y >= 1 && y <= 20; y++)
    if ((y%3) !== 0) {
        if ((y%2) === 0) {
        console.log (y, '- Berkualitas')
        }
        else
        console.log (y, '- Santai')
    }
    else console.log (y, '- I Love Coding')


// Soal 3
/*
########
########
########
######## 
*/

console.log('__Nomor 3__')
var z = "########"
for (x = 1; x <= 4; x++) {
    console.log(z)
}

//Soal 4
/*
#
##
###
####
#####
######
#######
*/

console.log('__Nomor 4__')

var tangga = ''
for (i = 1; i <=7; i++) {
    tangga += '#'
    console.log (tangga)
}


//Soal 5
/*
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
*/

console.log('__Nomor 5__')

var catur = ""
for (a = 0; a < 8; a++) {
    for (b = 0; b < 8; b++) {
        if (((a%2) == 0 && (b%2) == 0) || (((b%2)==1 && (a%2)==1))) {
         catur += ' '
        } 
        else {
         catur += '#'}
         
    }
    catur += '\n' 
}   
console.log(catur)
