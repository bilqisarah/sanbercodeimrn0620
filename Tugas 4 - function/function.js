//Soal 1
/*
    Tulis code function di sini
*/
console.log ('_Soal 1_') 


function teriak () {
    script = "Halo Sanbers!"
    return script
}

console.log(teriak()) 

//Soal 2
/*
Tulislah sebuah function dengan nama kalikan() yang mengembalikan 
hasil perkalian dua parameter yang di kirim.
*/

console.log ('_Soal 2_')

function kalikan(a,b) {
    script = a*b
    return script
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

//Soal 3
/*
Tulislah sebuah function dengan nama introduce() yang 
memproses paramater yang dikirim menjadi sebuah kalimat 
perkenalan seperti berikut: “Nama saya [name], umur saya [age] tahun, 
alamat saya di [address], dan saya punya hobby yaitu [hobby]!”
*/

console.log('_Soal 3_')
 
function introduce(p,q,r,s) {
    script = 'Nama saya ' + p + ", umur saya " + q + " tahun, alamat saya di " + r + ", dan saya punya hobby yaitu " + s + "!"
    return script
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)  