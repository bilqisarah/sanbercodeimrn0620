// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'Kidas', timeSpent: 1000}
]
 
// Tulis code untuk memanggil 
// function readBooks di sini

readBooks(10000, books[0], function sisaWaktu(waktu) {
    readBooks(waktu, books[1], function sisaWaktu1(waktu1) {
        readBooks(waktu1, books[2], function sisaWaktu2(waktu2) {
            readBooks(waktu2, books[3], function terakhir(x) {

            })
        })
    })
})



/*lanjutkan code pada index.js untuk memanggil function readBooks. 
Buku yang akan dihabiskan adalah buku-buku di dalam array books. 
Pertama function readBooks menerima input waktu yang dimiliki yaitu 10000 ms (10 detik) 
dan books pada indeks ke-0. Setelah mendapatkan callback sisa waktu yang dikirim lewat callback, 
sisa waktu tersebut dipakai untuk membaca buku pada indeks ke-1. 
Begitu seterusnya sampai waktu habis atau semua buku sudah terbaca. 
Untuk melihat output, jalankan file index.js dengan node js 
*/