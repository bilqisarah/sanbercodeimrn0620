var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'Kidas', timeSpent: 1000}
]
 

function read(times, index) {
    if (index < books.length) {
        readBooksPromise(times, books[index])
        .then(function(next) {return read(next, index + 1)})
        
        .catch(function(error) {
        })
    }
}

read(10000, 0)


// Lanjutkan code untuk menjalankan function readBooksPromise 
// Lakukan hal yang sama dengan soal no.1, 
// habiskan waktu selama 10000 ms (10 detik) untuk membaca semua buku dalam array books.!