// 1. Mengubah fungsi menjadi fungsi arrow

const golden = (goldenFunction) => {return "this is golden!!"};

console.log(golden())

// 2. Sederhanakan menjadi Object literal di ES6

let newFunction = (firstName, lastName) => {
    { let fullName = `${firstName} + " " + ${lastName}`
        return
    }  
}
console.log("William", "Imoh")

// 3. Destructuring
let full = {
 firstName1 : "Harry",
 lastName1 : "Potter Holt",
 destination : "Hogwarts React Conf",
 occupation : "Deve-wizard Avocado",
 spell : "Vimulus Renderus!!!"
}
  
// Driver code
const {firstName1, lastName1, destination, occupation} = full

console.log(firstName1, lastName1, destination, occupation)

// 4. Array Spreading
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east]

//Driver Code
console.log(combined)

// 5. Template Literals
const planet = 'earth'
const view = 'glass'
const before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 